<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([], function () use ($router) {
  $router->get('contentwrappers',  ['uses' => 'ContentWrapperController@index']);
  $router->get('contentwrappers/{id}', ['uses' => 'ContentWrapperController@show']);
  $router->put('contentwrappers/{id}', ['uses' => 'ContentWrapperController@update']);
  $router->post('contentwrappers', ['uses' => 'ContentWrapperController@store']);
  $router->delete('contentwrappers/{id}', ['uses' => 'ContentWrapperController@destroy']);
});

$router->group([], function () use ($router) {
  $router->get('mediaitems',  ['uses' => 'MediaItemController@index']);
  $router->get('mediaitems/{id}', ['uses' => 'MediaItemController@show']);
  $router->put('mediaitems/{id}', ['uses' => 'MediaItemController@update']);
  $router->post('mediaitems', ['uses' => 'MediaItemController@store']);
  $router->delete('mediaitems/{id}', ['uses' => 'MediaItemController@destroy']);
});

$router->group([], function () use ($router) {
  $router->get('pushtokens',  ['uses' => 'PushTokenController@index']);
  $router->post('pushtokens/sendToAll', ['uses' => 'PushTokenController@sendToAll']);
  $router->get('pushtokens/{id}', ['uses' => 'PushTokenController@show']);
  $router->put('pushtokens/{id}', ['uses' => 'PushTokenController@update']);
  $router->post('pushtokens', ['uses' => 'PushTokenController@store']);
  $router->delete('pushtokens/{id}', ['uses' => 'PushTokenController@destroy']);

});
