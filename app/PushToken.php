<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class PushToken extends Model
{
  protected $table = 'pushtoken';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deviceId', 'token'
    ];

}
