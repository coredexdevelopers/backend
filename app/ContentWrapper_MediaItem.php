<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ContentWrapper_MediaItem extends Model
{
  protected $table = 'contentwrapper_mediaitem';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contentWrapperId', 'mediaItemId'
    ];


}
