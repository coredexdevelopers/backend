<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ContentWrapper extends Model
{
  protected $table = 'contentwrapper';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description',
    ];

    protected $appends = array('newestMediaItem');

    public function mediaItems(){
      return $this->belongsToMany('App\MediaItem', 'contentwrapper_mediaitem',
      'contentWrapperId', 'mediaItemId');
    }

    public function getNewestMediaItemAttribute(){
      return $this->mediaItems()->orderBy('created_at', 'DESC')->first();
    }


}
