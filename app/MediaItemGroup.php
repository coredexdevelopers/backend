<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class MediaItemGroup extends Model
{
  protected $table = 'mediaitemgroup';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'groupMediaItemId', 'mediaItemId'
    ];


}
