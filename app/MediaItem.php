<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class MediaItem extends Model
{
  protected $table = 'mediaitem';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lowQualityAudio', 'highQualityAudio', 'video', 'pdf', 'title', 'description', 'thumbnail',
        'isGroup', 'recordedAt'
    ];

    public function contentWrappers(){
      return $this->belongsToMany('App\ContentWrapper', 'contentwrapper_mediaitem',
      'mediaItemId', 'contentWrapperId');
    }

    public function mediaItems(){
      return $this->belongsToMany('App\MediaItem', 'mediaitemgroup',
      'groupMediaItemId', 'mediaItemId');
    }
    public function mediaItemGroups(){
      return $this->hasMany('App\MediaItemGroup', 'mediaItemId');
    }
}
