<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\PushToken;
use Illuminate\Http\Request;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;

class PushTokenController extends Controller
{
  protected $request;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(){
      return PushToken::all();
    }

    public function store(Request $request){
      $pushToken = PushToken::create([
        'token' => $request->input('token'),
        'deviceId' => $request->input('deviceId')
      ]);
      return response()->json($pushToken, 201);
    }

    public function show(Request $request, $id){
      $pushToken = PushToken::find($id);
      if($pushToken == null){
        return response()->json($pushToken, 404);
      }
      return response()->json($pushToken);
    }

    public function update(Request $request, $id){
      $pushToken = PushToken::find($id);
      if($pushToken == null){
        return response()->json($pushToken, 404);
      }
      $pushToken->fill([
        'token' => $request->input('token', $pushToken->token),
        'deviceId' => $request->input('description', $pushToken->deviceId)
        ])->save();
      return response()->json($pushToken);
    }

    public function destroy(Request $request, $id){
      $pushToken = PushToken::find($id);
      if($pushToken == null){
        return response()->json($pushToken, 404);
      }
      $pushToken->delete();
      return response()->json($pushToken);
    }

    public function sendToAll(Request $request){ // sends a push notification to all app users
      $client = new Client();
      $body = $request->input('body');
      PushToken::chunk(30, function($tokens) use ($client, $body, $request) {



     
      foreach($tokens as $token){
        try {
          $response = $client->request('POST', env('PUSH_NOTIFICATIONS_HOST').'/notify.php', [
            'multipart' => [
              [
                'name' => 'body',
                'contents' => $body
              ],
              [
                'name' => 'deviceId',
                'contents' => $token->deviceId
              ],
              [
                'name' => 'token',
                'contents' => $token->token
              ],
              [
                'name' => 'data',
                'contents' => $request->input('data')
              ]
            ]
          ]);
           info("Notification sent successfully");
        } catch (\Exception $e){
         info("Failed to send notification");
// info($e);
        }
      }
});
    }

}
