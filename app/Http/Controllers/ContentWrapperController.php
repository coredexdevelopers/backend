<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\ContentWrapper;
use Illuminate\Http\Request;

class ContentWrapperController extends Controller
{
  protected $request;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(){
      return ContentWrapper::all();
    }

    public function store(Request $request){
      $contentWrapper = ContentWrapper::create([
        'title' => $request->input('title'),
        'description' => $request->input('description')
      ]);
      return response()->json($contentWrapper, 201);
    }

    public function show(Request $request, $id){
      $contentWrapper = ContentWrapper::find($id);
      if($contentWrapper == null){
        return response()->json($contentWrapper, 404);
      }
      return response()->json($contentWrapper);
    }

    public function update(Request $request, $id){
      $contentWrapper = ContentWrapper::find($id);
      if($contentWrapper == null){
        return response()->json($contentWrapper, 404);
      }
      $contentWrapper->fill([
        'title' => $request->input('title', $contentWrapper->title),
        'description' => $request->input('description', $contentWrapper->description)
        ])->save();
      return response()->json($contentWrapper);
    }

    public function destroy(Request $request, $id){
      $contentWrapper = ContentWrapper::find($id);
      if($contentWrapper == null){
        return response()->json($contentWrapper, 404);
      }
      $contentWrapper->delete();
      return response()->json($contentWrapper);
    }
}
