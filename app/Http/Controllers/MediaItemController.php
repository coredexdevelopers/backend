<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\MediaItem;
use App\MediaItemGroup;
use App\ContentWrapper;
use Illuminate\Http\Request;
use App\Standardizers\MediaItemFilter;
use App\Exceptions\StandardNotRespectedException;

class MediaItemController extends Controller
{
  protected $request;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(Request $request){
      $mediaItemFilter = new MediaItemFilter();
      try {
        $mediaItemFilter->setForeignFilter(json_decode($request->mediaItemFilter));
      } catch (StandardNotRespectedException $e){
        return response()->json($e, 400);
      }

      $mediaItems = MediaItem::query();

      if($mediaItemFilter->searchQuery){
        $mediaItems = $mediaItems->where('title', 'LIKE', '%'.$mediaItemFilter->searchQuery.'%')
        ->orWhere('description', 'LIKE', '%'.$mediaItemFilter->searchQuery.'%');
      }

      if($mediaItemFilter->groupMediaItemId){
        $mediaItems = $mediaItems->whereHas('mediaItemGroups', function($query) use ($mediaItemFilter){
          $query->where('groupMediaItemId', '=', $mediaItemFilter->groupMediaItemId);
        });
      }

      if($mediaItemFilter->contentWrapperId){
        $mediaItems = $mediaItems->whereHas('contentWrappers', function($query) use ($mediaItemFilter){
          $query->where('contentwrapper.id', '=', $mediaItemFilter->contentWrapperId);
        });
      }
      if(!$mediaItemFilter->includeVideo){
        $mediaItems = $mediaItems->whereNull('video');
      }
      if(!$mediaItemFilter->includeAudio){
        $mediaItems = $mediaItems->whereNull('lowQualityAudio');
        $mediaItems = $mediaItems->whereNull('highQualityAudio');
      }
      if(!$mediaItemFilter->includePdf){
        $mediaItems = $mediaItems->whereNull('pdf');
      }

      return response()->json($mediaItems->orderBy($mediaItemFilter->orderBy, $mediaItemFilter->reverse?'DESC':'ASC')->get());
    }
    public function store(Request $request){
      if($request->input('groupMediaItemId')){
        $mediaItemGroup = MediaItem::find($request->input('groupMediaItemId'));
        if(!$mediaItemGroup){
          return response()->json($mediaItemGroup, 400);
        }
      }
      if($request->input('contentWrapperId')){
        $contentWrapper = ContentWrapper::find($request->input('contentWrapperId'));
        if(!$contentWrapper){
          return response()->json($contentWrapper, 400);
        }
      }

      $mediaItem = MediaItem::create([
        'lowQualityAudio' => $request->input('lowQualityAudio'),
        'highQualityAudio' => $request->input('highQualityAudio'),
        'video' => $request->input('video'),
        'pdf' => $request->input('pdf'),
        'title' => $request->input('title'),
        'description' => $request->input('description'),
        'thumbnail' => $request->input('thumbnail'),
        'isGroup' => $request->input('isGroup', 0),
        'recordedAt' => $request->input('recordedAt', Carbon::today())
      ]);
      if($request->input('contentWrapperId')){
        $contentWrapper->mediaItems()->attach($mediaItem->id);
      }
      if($request->input('groupMediaItemId')){
        $mediaItemGroup->mediaItems()->attach($mediaItem->id);
      }
      return response()->json($mediaItem, 201);
    }

    public function show(Request $request, $id){
      $mediaItem = MediaItem::find($id);
      if($mediaItem == null){
        return response()->json($mediaItem, 404);
      }
      return response()->json($mediaItem);
    }

    public function update(Request $request, $id){
      if($request->input('groupMediaItemId')){
        $mediaItemGroup = MediaItem::find($request->input('groupMediaItemId'));
        if(!$mediaItemGroup){
          return response()->json($mediaItemGroup, 400);
        }
      }
      if($request->input('contentWrapperId')){
        $contentWrapper = ContentWrapper::find($request->input('contentWrapperId'));
        if(!$contentWrapper){
          return response()->json($contentWrapper, 400);
        }
      }
      $mediaItem = MediaItem::find($id);
      if($mediaItem == null){
        return response()->json($mediaItem, 404);
      }
      $mediaItem->fill([
        'lowQualityAudio' => $request->input('lowQualityAudio', $mediaItem->lowQualityAudio),
        'highQualityAudio' => $request->input('highQualityAudio', $mediaItem->highQualityAudio),
        'video' => $request->input('video', $mediaItem->video),
        'pdf' => $request->input('pdf', $mediaItem->pdf),
        'title' => $request->input('title'),
        'description' => $request->input('description', $mediaItem->description),
        'thumbnail' => $request->input('thumbnail', $mediaItem->thumbnail),
        'isGroup' => $request->input('isGroup', $mediaItem->isGroup),
        'recordedAt' => $request->input('recordedAt', $mediaItem->recordedAt)
        ])->save();
        if($request->input('contentWrapperId')){
          $contentWrapper->mediaItems()->attach($mediaItem->id);
        }
        if($request->input('groupMediaItemId')){
          $mediaItemGroup->mediaItems()->attach($mediaItem->id);
        }
      return response()->json($mediaItem);
    }

    public function destroy(Request $request, $id){
      $mediaItem = MediaItem::find($id);
      if($mediaItem == null){
        return response()->json($mediaItem, 404);
      }
      $mediaItem->delete();
      return response()->json($mediaItem);
    }
}
