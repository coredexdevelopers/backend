<?php

namespace App\Standardizers;
use App\Exceptions\StandardNotRespectedException;

class MediaItemFilter
{
  public $includeVideo = true;
  public $includeAudio = true;
  public $includePdf = true;
  public $groupMediaItemId = null;
  public $contentWrapperId = null;
  public $searchQuery = null;
  public $orderBy = 'id';
  public $reverse = 0;

  private function isStandard($foreignMediaItemFilter){
    try {
      $foreignProperties = get_object_vars($foreignMediaItemFilter);
      $standardProperties = get_object_vars($this);
      return count(array_diff_key($foreignProperties, $standardProperties))==0;
    } catch(\Exception $e){
      return false;
    }

  }

  public function setForeignFilter($foreignMediaItemFilter){
    if(!$this->isStandard($foreignMediaItemFilter)){
      throw new StandardNotRespectedException();
    }
    foreach (get_object_vars($foreignMediaItemFilter) as $key => $value) {
        $this->$key = $value;
    }
  }
}
