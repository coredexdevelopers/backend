<?php
namespace App\Exceptions;

use Exception;

class StandardNotRespectedException extends Exception
{
    public $message = "The filter object does not respect the standard defined in Standardizers. Check the project's README for more details.";
}
