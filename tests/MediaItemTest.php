<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\MediaItem;
use App\MediaItemGroup;
use App\ContentWrapper_MediaItem;
use App\ContentWrapper;

class MediaItemTest extends TestCase
{

    public function testAddMediaItemToNonExistingGroup()
    {
      // Expected result: 400, no row added to mediaitem and no row added to mediaitemgroup
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousMediaItemGroupsCount = MediaItemGroup::all()->count();

      $data = [
        'groupMediaItemId' => 9999,
      ];
      $response = $this->call('POST', '/mediaitems', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentMediaItemGroupsCount = MediaItemGroup::all()->count();
      $this->assertEquals($previousMediaItemsCount, $currentMediaItemsCount);
      $this->assertEquals($currentMediaItemGroupsCount, $previousMediaItemGroupsCount);
      $this->assertEquals(400, $response->status());
    }

    public function testAddMediaItemToExistingGroup()
    {
      // Expected result: 201, 1 row added to mediaitem and 1 row added to mediaitemgroup
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousMediaItemGroupsCount = MediaItemGroup::all()->count();
      $mediaItemGroup = MediaItem::where('isGroup', '=', 1)->first();
      $data = [
        'groupMediaItemId' => $mediaItemGroup->id,
        'title' => 'Test Media Item'
      ];
      $response = $this->call('POST', '/mediaitems', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentMediaItemGroupsCount = MediaItemGroup::all()->count();
      $this->assertEquals(201, $response->status());
      $this->assertEquals($previousMediaItemsCount+1, $currentMediaItemsCount);
      $this->assertEquals($currentMediaItemGroupsCount, $previousMediaItemGroupsCount+1);
    }

    public function testAddMediaItemToNoGroup()
    {
      // Expected result: 201, 1 row added to mediaitem and 0 row added to mediaitemgroup
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousMediaItemGroupsCount = MediaItemGroup::all()->count();
      $mediaItemGroup = MediaItem::where('isGroup', '=', 1)->first();
      $data = [
        'title' => 'Test Media Item'
      ];
      $response = $this->call('POST', '/mediaitems', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentMediaItemGroupsCount = MediaItemGroup::all()->count();
      $this->assertEquals(201, $response->status());
      $this->assertEquals($previousMediaItemsCount+1, $currentMediaItemsCount);
      $this->assertEquals($currentMediaItemGroupsCount, $previousMediaItemGroupsCount);
    }

    public function testUpdateMediaItemToExistingGroup()
    {
      // Expected result: 200, 0 row added to mediaitem, 1 row added to mediaitemgroup
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousMediaItemGroupsCount = MediaItemGroup::all()->count();
      $mediaItemGroup = MediaItem::where('isGroup', '=', 1)->first();
      $data = [
        'groupMediaItemId' => $mediaItemGroup->id,
        'title' => 'Test Media Item'
      ];
      $response = $this->call('PUT', '/mediaitems/2', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentMediaItemGroupsCount = MediaItemGroup::all()->count();
      $this->assertEquals(200, $response->status());
      $this->assertEquals($previousMediaItemsCount, $currentMediaItemsCount);
      $this->assertEquals($currentMediaItemGroupsCount, $previousMediaItemGroupsCount+1);
    }

    public function testUpdateMediaItemToNonExistingGroup()
    {
      // Expected result: 400, 0 row added to mediaitem, 0 row added to mediaitemgroup
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousMediaItemGroupsCount = MediaItemGroup::all()->count();
      $mediaItemGroup = MediaItem::where('isGroup', '=', 1)->first();
      $data = [
        'groupMediaItemId' => 9999,
        'title' => 'Test Media Item'
      ];
      $response = $this->call('PUT', '/mediaitems/2', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentMediaItemGroupsCount = MediaItemGroup::all()->count();
      $this->assertEquals(400, $response->status());
      $this->assertEquals($previousMediaItemsCount, $currentMediaItemsCount);
      $this->assertEquals($currentMediaItemGroupsCount, $previousMediaItemGroupsCount);
    }

    // Part 2: testing with contentwrapper
    public function testAddMediaItemToNonExistingContentWrapper()
    {
      // Expected result: 400, no row added to mediaitem and no row added to contentwrapper_mediaitem
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();

      $data = [
        'contentWrapperId' => 9999,
      ];
      $response = $this->call('POST', '/mediaitems', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $this->assertEquals($previousMediaItemsCount, $currentMediaItemsCount);
      $this->assertEquals($currentContentWrapperMediaItemsCount, $previousContentWrapperMediaItemsCount);
      $this->assertEquals(400, $response->status());
    }

    public function testAddMediaItemToExistingContentWrapper()
    {
      // Expected result: 201, 1 row added to mediaitem and 1 row added to contentwrapper_mediaitem
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $contentWrapper = ContentWrapper::all()->first();
      $data = [
        'contentWrapperId' => $contentWrapper->id,
        'title' => 'Test Media Item'
      ];
      $response = $this->call('POST', '/mediaitems', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $this->assertEquals(201, $response->status());
      $this->assertEquals($previousMediaItemsCount+1, $currentMediaItemsCount);
      $this->assertEquals($currentContentWrapperMediaItemsCount, $previousContentWrapperMediaItemsCount+1);
    }

    public function testAddMediaItemToNoContentWrapper()
    {
      // Expected result: 201, 1 row added to mediaitem and 0 row added to contentwrapper_mediaitem
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $data = [
        'title' => 'Test Media Item'
      ];
      $response = $this->call('POST', '/mediaitems', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $this->assertEquals(201, $response->status());
      $this->assertEquals($previousMediaItemsCount+1, $currentMediaItemsCount);
      $this->assertEquals($currentContentWrapperMediaItemsCount, $previousContentWrapperMediaItemsCount);
    }

    public function testUpdateMediaItemToExistingContentWrapper()
    {
      // Expected result: 200, 0 row added to mediaitem, 1 row added to currentwrapper_mediaitem
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $contentWrapper = ContentWrapper::all()->first();
      $data = [
        'contentWrapperId' => $contentWrapper->id,
        'title' => 'Test Media Item'
      ];
      $response = $this->call('PUT', '/mediaitems/2', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $this->assertEquals(200, $response->status());
      $this->assertEquals($previousMediaItemsCount, $currentMediaItemsCount);
      $this->assertEquals($currentContentWrapperMediaItemsCount, $previousContentWrapperMediaItemsCount+1);
    }

    public function testUpdateMediaItemToNonExistingContentWrapper()
    {
      // Expected result: 400, 0 row added to mediaitem, 0 row added to contentwrapper_mediaitem
      $previousMediaItemsCount = MediaItem::all()->count();
      $previousContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $data = [
        'contentWrapperId' => 9999,
        'title' => 'Test Media Item'
      ];
      $response = $this->call('PUT', '/mediaitems/2', $data);

      $currentMediaItemsCount = MediaItem::all()->count();
      $currentContentWrapperMediaItemsCount = ContentWrapper_MediaItem::all()->count();
      $this->assertEquals(400, $response->status());
      $this->assertEquals($previousMediaItemsCount, $currentMediaItemsCount);
      $this->assertEquals($currentContentWrapperMediaItemsCount, $previousContentWrapperMediaItemsCount);
    }

    public function testGetMediaItemsOfNonExistingContentWrapper(){
      // Expected result: 200, return an empty set
      $data = [
        'contentWrapperId' => 9999
      ];
      $response = $this->call('GET', '/mediaitems?mediaItemFilter='.json_encode($data));

      $mediaItems = MediaItem::where('id', '>=', 0);
      $mediaItemsCount = $mediaItems->whereHas('contentWrappers', function($query){
        $query->where('contentwrapper.id', '=', 9999);
      })->count();

      $this->assertEquals(200, $response->status());
      $this->assertEquals($mediaItemsCount, count(json_decode($response->getContent())));
    }

    public function testGetMediaItemsOfContentWrapper(){
      // Expected result: 200, return an non-empty set (must be improved)
      $data = [
        'contentWrapperId' => 1
      ];
      $response = $this->call('GET', '/mediaitems?mediaItemFilter='.json_encode($data));

      $mediaItems = MediaItem::where('id', '>=', 0);
      $mediaItemsCount = $mediaItems->whereHas('contentWrappers', function($query){
        $query->where('contentwrapper.id', '=', 1);
      })->count();

      $this->assertEquals(200, $response->status());
      $this->assertEquals($mediaItemsCount, count(json_decode($response->getContent())));
    }

    public function testGetMediaItemsOfNonExistingGroup(){
      // Expected result: 200, return empty set
      $data = [
        'groupMediaItemId' => 9999
      ];
      $response = $this->call('GET', '/mediaitems?mediaItemFilter='.json_encode($data));

      $this->assertEquals(0, count(json_decode($response->getContent())));
      $this->assertEquals(200, $response->status());
    }

    public function testGetMediaItemsOfExistingGroup(){
      // Expected result: 200, with count returned same as count from db query
      $data = [
        'groupMediaItemId' => 2
      ];
      $response = $this->call('GET', '/mediaitems?mediaItemFilter='.json_encode($data));

      $mediaItems = MediaItem::where('id', '>=', 0);
      $mediaItemsCount = $mediaItems->whereHas('mediaItemGroups', function($query){
        $query->where('groupMediaItemId', '=', 2);
      })->count();

      $this->assertEquals($mediaItemsCount, count(json_decode($response->getContent())));
      $this->assertEquals(200, $response->status());
    }


}
