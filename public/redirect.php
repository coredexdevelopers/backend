<?php
$location = $_GET['location'];
if(isset($_GET['redirect'])){
    header("location: $location");
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dr.Salem</title>
<style>
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

body {
  width: 100%;
  background-image: url("splash.png");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  align-items: flex-end;
  justify-content: center;
}

.google-play-button {
  width: 300px;
}
</style>
</head>
<body>
  <img src="google-play-badge.png" class="google-play-button" onclick="window.open('https://play.google.com/store/apps/details?id=net.coredex.sheikhapp&hl=en')" />
</body>
<script>
document.querySelector('.google-play-button').style.display = 'none';
setTimeout(() => {document.querySelector('.google-play-button').style.display = 'block'}, 2000);
setTimeout(() => {
window.location.href = 'http://dr-salemalrafei.com/backend/public/redirect.php?location=<?php echo $location ?>&redirect=1';
}, 2000);
</script>
</html>
