<?php
error_reporting(E_ALL);
$file = $_GET['file']; // a remote file url
//print_r(get_headers($file));


$headers = array_change_key_case(get_headers(encodeExistingURL($file), TRUE));
$filesize = $headers['content-length'];


if ( $headers[0] != 'HTTP/1.1 404 Not Found' ) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Length: ' . $filesize);
    ob_end_clean();
    flush();
    readfile(encodeExistingURL($file));
    exit;
}

while(1) {
    Echo "\n";
    if ( connection_status() != 0 ) {
        die;
    }
}


function encodeExistingURL($input) {

    //From: https://en.wikipedia.org/wiki/Percent-encoding#Types_of_URI_characters
    //The percent sign is included to leave existing encoded characters intact.
    $valid  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~!*\'();:@&=+$,/?#[]%';
    $length = strlen($input);
    for ($i = 0; $i < $length; $i++) {
        $character = $input[$i];
        $output   .= (strpos($valid, $character) === FALSE ? rawurlencode($character) : $character);
    }
    return $output;

}
?>
