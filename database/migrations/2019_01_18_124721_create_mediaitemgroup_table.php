<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaitemgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mediaitemgroup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groupMediaItemId')->unsigned();
            $table->foreign('groupMediaItemId')->references('id')->on('mediaitem')->onDelete('RESTRICT');
            $table->integer('mediaItemId')->unsigned();
            $table->foreign('mediaItemId')->references('id')->on('mediaitem')->onDelete('CASCADE');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediaitemgroup');
    }
}
