<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentwrapperMediaitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contentwrapper_mediaitem', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contentWrapperId')->unsigned();
            $table->foreign('contentWrapperId')->references('id')->on('contentwrapper')->onDelete('RESTRICT');
            $table->integer('mediaItemId')->unsigned();
            $table->foreign('mediaItemId')->references('id')->on('mediaitem')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contentwrapper_mediaitem');
    }
}
