<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mediaitem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lowQualityAudio')->nullable();
            $table->string('highQualityAudio')->nullable();
            $table->string('video')->nullable();
            $table->string('pdf')->nullable();
            $table->string('title');
            $table->string('description')->nullable();
            $table->boolean('isGroup');
            $table->datetime('recordedAt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediaitem');
    }
}
