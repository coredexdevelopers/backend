<?php

use Illuminate\Database\Seeder;

class MediaItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for($i=0; $i<50; $i++){
        DB::table('mediaitem')->insert([
            'lowQualityAudio' => str_random(10).'mp3',
            'highQualityAudio' => str_random(10).'mp3',
            'video' => str_random(10).'mp4',
            'pdf' => str_random(10).'pdf',
            'title' => str_random(10),
            'description' => str_random(10),
            'isGroup' => array_random([0, 1])
        ]);
      }
    }
}
