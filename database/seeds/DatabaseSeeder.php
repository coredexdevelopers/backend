<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
          ContentWrapperTableSeeder::class,
          MediaItemTableSeeder::class,
          MediaItemGroupTableSeeder::class,
          ContentWrapper_MediaItemTableSeeder::class
        ]);
    }
}
