<?php

use Illuminate\Database\Seeder;

class MediaItemGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $mediaItemIds = DB::table('mediaitem')->where('isGroup', '=', 0)->pluck('id')->all();
      $mediaItemGroupIds = DB::table('mediaitem')->where('isGroup', '=', 1)->pluck('id')->all();

      for($i=0; $i<50; $i++){
        DB::table('mediaitemgroup')->insert([
            'groupMediaItemId' => array_random($mediaItemGroupIds),
            'mediaItemId' => array_random($mediaItemIds)
        ]);
      }
    }
}
