<?php

use Illuminate\Database\Seeder;

class ContentWrapper_MediaItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $mediaItemIds = DB::table('mediaitem')->pluck('id')->all();
      $contentWrapperIds = DB::table('contentwrapper')->pluck('id')->all();
      for($i=0; $i<50; $i++){
        DB::table('contentwrapper_mediaitem')->insert([
            'contentWrapperId' => array_random($contentWrapperIds),
            'mediaItemId' => array_random($mediaItemIds)
        ]);
      }
    }
}
