# Sheikh App Backend

## Setup

Prerequisites are PHP 7.2+ and MySQL.  

1- Clone the repo.  
2- Run `composer update`.  
3- Set up your copy of the .env file.  
4- Run `php artisan migrate` to create the database tables.  
5- Run `php artisan db:seed` to fill the table with dummy data.  
6- Run `./vendor/bin/phpunit` to test that everything is ok.  
7- Run `php -S localhost:<port> -t public` to run the server at <port>.  

## Coding Style and Rules
- Database names shall be fully concatenated.  
    - Bad: `mediaItem` (camelcase)
    - Bad: `media_item` (snake case)
    - Good: `mediaitem`
- Database pivot table names composed of two or more table names must be separated with an underscore.
This happens in the case of many-to-many relationships.
    - Bad: `mediaitemphoto`
    - Bad: `mediaitemPhoto`
    - Good: `mediaitem_photo`
- Database table attributes shall be written in camel case (except for Lumen's auto-generated timestamp attributes).
    - Bad: `mediaItemID`
    - Bad: `mediaitem_id`
    - Good: `mediaItemId`
- Optional table attributes must be nullable.
- Database table names shall be explicitly written in their respective Lumen model.
- `201` must be returned when a new resource is created alongside the resource in JSON format.
- `404` must be returned when a request to get or modify a non-existent resource is made.
- `400` must be returned when a request to get or modify an existing resource but with related
resources that could not be found.  
Example: user requests a Car with wheel number 5. The car is there, but there is no wheel number 5.  
In this case, return 400 with the result of the query for wheel number 5 (usually null, but return it anyway).
- `200` must be returned when a request to get or modify an existing resource is successfully fulfilled.


## Database Structure
TODO



## Testing
TODO

## Standardizers
TODO
